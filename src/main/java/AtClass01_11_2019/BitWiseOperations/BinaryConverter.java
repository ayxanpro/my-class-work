package AtClass01_11_2019.BitWiseOperations;

public class BinaryConverter {
  private static void convertToBinary(int a) {
    for (int i = 7; i >= 0; i--) {
      //System.out.printf("%d ",((1 << i) & a) != 0 ? 1 : 0);
      System.out.printf("%d ", (a >> i & 1));
    }
  }

  private static void convertToDecimal(String seq) {
    int res = 0;
    for (int i = 0; i < seq.length(); i++) {
      res += Integer.parseInt(seq.charAt(i) + "") << (seq.length() - i - 1);
    }
    System.out.println(res);
  }

  public static void main(String[] args) {
    convertToBinary(73);
    System.out.println();
    convertToDecimal("01110110");
  }
}
