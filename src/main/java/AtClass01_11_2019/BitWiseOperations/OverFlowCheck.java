package AtClass01_11_2019.BitWiseOperations;

public class OverFlowCheck {
  static long add1(long a, long b) {
    if ((a | b) == Long.MAX_VALUE) throw new IllegalStateException("Overflow");
    return a + b;
  }

  static long add2(long a, long b) {
    long sa = a >> 63;
    long sb = b >> 63;
    long res = ~(sa ^ sb);
    //System.out.println(res);
    if (res == -1) throw new IllegalStateException("Overflow expected");
    return a + b;
  }

  public static void main(String[] args) {
    long c1 = add1(Long.MAX_VALUE - 1, 131);
//                long c2 = add2(-Long.MAX_VALUE, -131);
    long c2 = add1(123123, 2123);
//                System.out.println(c1);
    System.out.println(c2);
  }
}
