package AtClass01_11_2019.BitWiseOperations;

public class Testing {
  public static void main(String[] args) {
    int a = 0x0011;
    int b = 0x0101;
    int or = a | b;
    int and = a & b;
    int xor = a ^ b;
    int wavyA = ~a;
    int wavyB = ~b;
    System.out.println(or);
    System.out.println(and);
    System.out.println(xor);
    System.out.println(wavyA);
    System.out.println(wavyB);
  }
}
