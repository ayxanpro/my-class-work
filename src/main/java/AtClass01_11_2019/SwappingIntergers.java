package AtClass01_11_2019;

public class SwappingIntergers {
  public static void swapWithOutThird(int a, int b) {
    System.out.printf("a=%d, b=%d\n", a, b);
    a = a + b;
    b = a - b;
    a = a - b;
    System.out.printf("a=%d, b=%d\n", a, b);
  }

  public static void main(String[] args) {
    swapWithOutThird(34, 43);
  }
}
