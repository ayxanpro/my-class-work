package AtClass02_12_2019.warmup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WarmUpSolution {
  public static void main(String[] args) {
    new WarmUpSolution().solve(new int[][]{{1, 6, 3}, {}, {5, 4}, {7, 8, 2, 9}, {}});
    ArrayList<Integer> list1 = new ArrayList<Integer>() {{
      add(3);
      add(31);
      add(23);
      add(1);
      add(14);
      add(4);
    }};
    ArrayList<Integer> list2 = new ArrayList<Integer>() {{
      add(2);
      add(4);
      add(12);
      add(3);
      add(123);
      add(54);
    }};

    ArrayList<ArrayList<Integer>> arrayLists = new ArrayList<ArrayList<Integer>>() {{
      add(list1);
      add(list2);
    }};

    Stream<Object> objectStream = flatMap(arrayLists.stream().map(el -> el.stream()));
    objectStream.forEach(el -> System.out.printf("%s,", el));
  }

  public static Stream<Object> flatMap(Stream<? extends Stream> streams) {
    ArrayList<Object> res = new ArrayList<>();
    streams.forEach(stream -> res.addAll(
        (ArrayList) stream.collect(Collectors.toList())
    ));
    return res.stream();
  }

  public void solve(int[][] arr) {
    List<int[]> ints = Arrays.asList(arr);

    List<Integer> sortedList = ints
        .stream()
        .flatMapToInt(Arrays::stream)
        .boxed()
        .sorted((o1, o2) -> o1 - o2)
        .collect(Collectors.toList());

    IntSummaryStatistics intSummaryStatistics = sortedList.stream().mapToInt(x -> x).summaryStatistics();

    double average = intSummaryStatistics.getAverage();
    int max = intSummaryStatistics.getMax();
    int min = intSummaryStatistics.getMin();
    long count = intSummaryStatistics.getCount();
    long sum = intSummaryStatistics.getSum();

    System.out.printf("Sorted array: %s\n", sortedList);
    System.out.printf("Max element: %s\n", max);
    System.out.printf("Min element: %s\n", min);
    System.out.printf("Number of elements: %s\n", count);
    System.out.printf("Average: %s\n", average);
    System.out.printf("Sum of all: %s\n", sum);
  }
}
