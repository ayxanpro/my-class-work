package AtClass04_11_2019;

import java.util.ArrayDeque;
import java.util.Deque;

public class BracketNestingCalculator {
  public int calc(String str) {
    int res = 0;
    Deque<Character> st = new ArrayDeque<>();
    for (int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      if (c == '(') {
        st.push(c);
        res = Math.max(res, st.size());
      } else if (c == ')') {
        if (!st.isEmpty())
          st.pop();
      }
    }
    return res;
  }
}
