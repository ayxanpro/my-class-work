package AtClass04_11_2019;

import java.util.ArrayDeque;
import java.util.Deque;

public class FindingMissingBrackets {
  public int calc(String str) {
    int closing = 0;
    int opening = 0;
    int res = 0;
    Deque<Character> st = new ArrayDeque<>();
    for (int i = 0; i < str.length(); i++) {
      char c = str.charAt(i);
      if (c == '(') {
        st.push(c);
        res = Math.max(res, st.size());
      } else if (c == ')') {
        if (!st.isEmpty()) st.pop();
        else {
          opening++;
        }
      }
    }
    closing = st.size();
    if (closing != 0 && opening != 0) {
      throw new IllegalArgumentException(String.format("Missing %d:(, %d:)", closing, opening));
    }
    return res;
  }

}
