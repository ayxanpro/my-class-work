package AtClass04_11_2019.GenericMergeSort;

import java.util.Arrays;

public class MergeApp {
  public static void main(String[] args) {
    MergeSort<String> sda = new MergeSort<>();
    String[] init = {"asdasdasd", "asdasdawdwqdfsd", "faasddsa", "sadsadsad", "asdsadaw", "sdawda"};
    System.out.println(Arrays.toString(init));
    sda.sort(init, 0, init.length - 1, (o1, o2) -> o1.compareTo(o2));
    System.out.println(Arrays.toString(init));
    sda.sort(init, 0, init.length - 1, (o1, o2) -> o1.length() - o2.length());
    System.out.println(Arrays.toString(init));
  }
}
