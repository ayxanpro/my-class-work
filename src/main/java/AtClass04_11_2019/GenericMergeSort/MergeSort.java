package AtClass04_11_2019.GenericMergeSort;

import java.util.ArrayList;
import java.util.Comparator;

public class MergeSort<T> {
  void merge(T[] arr, int l_index, int middle_index, int r_index, Comparator<T> comp) {
    int size_l = middle_index - l_index + 1;
    int size_r = r_index - middle_index;

    ArrayList<T> left = new ArrayList<>(size_l);
    ArrayList<T> right = new ArrayList<>(size_r);

    // Copy
    for (int i = 0; i < size_l; ++i) {
      left.add(i, arr[l_index + i]);
    }
    for (int j = 0; j < size_r; ++j) {
      right.add(j, arr[middle_index + 1 + j]);
    }

    // Merge
    int i = 0, j = 0;
    // Initial index of merged sub-array
    int k = l_index;
    while (i < size_l && j < size_r) {
      if (comp.compare(left.get(i), right.get(j)) <= 0) {
        arr[k] = left.get(i++);
      } else {
        arr[k] = right.get(j++);
      }
      k++;
    }

    while (i < size_l) {
      arr[k++] = left.get(i++);
    }

    while (j < size_r) {
      arr[k++] = right.get(j++);
    }
  }

  void sort(T[] arr, int l, int r, Comparator<T> comp) {
    if (l < r) {
      int m = (l + r) / 2;
      sort(arr, l, m, comp);
      sort(arr, m + 1, r, comp);
      merge(arr, l, m, r, comp);
    }
  }
}
