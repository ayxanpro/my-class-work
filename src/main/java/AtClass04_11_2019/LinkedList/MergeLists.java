package AtClass04_11_2019.LinkedList;

public class MergeLists {
  public XLinkedList.XItem merge(XLinkedList list1, XLinkedList list2) {
    XLinkedList.XItem head1 = list1.getHead();
    XLinkedList.XItem head2 = list2.getHead();

    XLinkedList.XItem head = null;
    XLinkedList.XItem tail = null;

    if (head1.value <= head2.value) {
      head = head1;
      head1 = head1.next;
    } else {
      head = head2;
      head2 = head2.next;
    }
    tail = head;

    while (head1 != null && head2 != null) {
      if (head1.value <= head2.value) {
        tail.next = head1;
        head1 = head1.next;
      } else {
        tail.next = head2;
        head2 = head2.next;
      }
      tail = tail.next;
    }

    if (head1 != null) {
      tail.next = head1;
    }

    if (head2 != null) {
      tail.next = head2;
    }
    return head;
  }
}
