package AtClass04_11_2019.LinkedList;

import java.util.StringJoiner;

public class XLinkedList {
  private XItem head;

  class XItem {
    int value;
    XItem next;

    XItem(int value) {
      this.value = value;
    }

    public String toString() {
      return String.valueOf(this.value);
    }
  }

  public XLinkedList() {

  }

  public XLinkedList(XItem head) {
    this.head = head;
  }

  public XItem getHead() {
    return head;
  }

  public void add(int value) {
    XItem item = new XItem(value);
    if (head == null) {
      head = item;
    } else {
      XItem current = head;

      while (current.next != null) {
        current = current.next;
      }
      current.next = item;
    }
  }

  public int length() {
    int res = 0;
    for (XItem curr = this.head; curr != null; curr = curr.next, res++) ;
    return res;
  }

  public int recursiveLength(XItem curr) {
    return curr == null ? 0 : recursiveLength(curr.next) + 1;
  }

  public int length2() {
    return recursiveLength(head);
  }

  public int recursiveLength3(XItem curr, int size) {
    if (curr == null) return size;
    return recursiveLength3(curr.next, size + 1);
  }

  public int length3() {
    return recursiveLength3(head, 0);
  }

  public boolean contains(int value) {
    XItem temp = head;
    while (temp != null) {
      if (temp.value == value) return true;
      temp = temp.next;
    }
    return false;
  }

  public String toString() {
    StringJoiner sj = new StringJoiner(",", "[", "]");
    XItem temp = head;
    while (temp != null) {
      sj.add(String.valueOf(temp.value));
      temp = temp.next;
    }
    return sj.toString();
  }

  public void remove(int value) {
    if (head == null) return;
    if (head.value == value) { //if value is at head
      head = head.next;
      return;
    }

    XItem curr = head;
    while (curr != null && curr.next.value != value) {
      curr = curr.next;
    }
    curr.next = curr.next.next;
  }

  public void revert() {
    XItem head = null;
    XItem curr = this.head;

    while (curr != null) {

      XItem temp = curr;
      curr = curr.next;
      temp.next = null;
      temp.next = head;
      head = temp;

    }
    this.head = head;
  }

  private void rev_rec(XItem curr, XItem head) {
    if (curr == null) {
      this.head = head;
      return;
    }

    XItem temp = curr;
    curr = curr.next;
    temp.next = null;
    temp.next = head;
    head = temp;

    rev_rec(curr, head);
  }

  public void revert_with_recursion() {
    XItem curr = this.head;
    rev_rec(curr, null);
  }

  public void revertTwoLoops() {
    XLinkedList temp = new XLinkedList();
    while (head.next != null) {
      XItem curr = head;
      while (curr.next.next != null) {
        curr = curr.next;
      }
      temp.add(curr.next.value);
      curr.next = null;
    }
    temp.add(head.value);
    this.head = temp.head;
  }


}
