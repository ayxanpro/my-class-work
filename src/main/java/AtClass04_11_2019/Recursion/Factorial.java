package AtClass04_11_2019.Recursion;

public class Factorial {

  public static void main(String[] args) {
    Factorial app = new Factorial();
    int f5 = app.fact(5);
    System.out.println(f5);
  }

  private int fact(int i) {
    return i == 1 ? 1 : i * fact(i - 1);
  }
}
