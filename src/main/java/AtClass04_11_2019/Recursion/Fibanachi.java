package AtClass04_11_2019.Recursion;

import java.util.ArrayList;

public class Fibanachi {
  public static void main(String[] args) {
    Fibanachi app = new Fibanachi();
    System.out.println(app.usingArr(55));
//                int res = app.fib(5);
//                System.out.println(res);
//                for (int i = 1; i < 20; i++) {
//                        System.out.println(i + " = " + app.fib(i));
//                }
  }

  private long usingArr(int n) {
    ArrayList<Long> fibs = new ArrayList<>();
    fibs.add((long) 1);
    fibs.add((long) 1);
    for (int i = 2; i < n; i++) {
      fibs.add(fibs.get(i - 1) + fibs.get(i - 2));
    }
    return fibs.get(n - 1);
  }

  private int fibIf(int n) {
    if (n == 1 || n == 2) return 1;
    return fibIf(n - 1) + fibIf(n - 2);
  }

  private int fib(int n) {
    return n == 1 || n == 2 ? 1 : fib(n - 1) + fib(n - 2);
  }
}
