package AtClass04_12_2019.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionApp {
  public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
    Point3D point3D = new Point3D(1, 2, 3);
    point3D.printMe();

    String class_name = "AtClass04_12_2019.reflection.Point3D";
    Class<?> aClass = Class.forName(class_name);
    Constructor<?> constructor = aClass.getConstructor(int.class, int.class, int.class);
    Point3D o = (Point3D) constructor.newInstance(10, 20, 30);
    o.printMe();
  }
}
