package AtClass04_12_2019.warmup;

import java.util.stream.Collectors;

public class Solution {
  public String solve(String str) {
    return str.chars()
        .distinct()
        .mapToObj(x -> String.valueOf((char) x))
        .collect(Collectors.joining());
  }
}
