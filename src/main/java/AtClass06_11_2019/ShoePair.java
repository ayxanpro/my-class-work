package AtClass06_11_2019;

public class ShoePair {
  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.solution("RLRRLLRLRRLL"));
    System.out.println(s.solution("LLRLRLRLRLRLRR"));
  }
}


class Solution {
  public int solution(String s) {
    int r = 0;
    int l = 0;
    int res = 0;
    for (int i = 0; i < s.length(); i++) {
      switch (s.charAt(i)) {
        case 'R':
          r++;
          break;
        case 'L':
          l++;
          break;
        default:
          System.out.println("unknown");
          continue;
      }
      if (r == l) res++;
    }
    return res;
  }
}