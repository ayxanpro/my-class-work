package AtClass06_12_2019;

import java.util.Comparator;
import java.util.Random;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class WarmUp {
  public static void main(String[] args) {
    Random r = new Random();
    StringJoiner arr = new StringJoiner(",", "[", "]");
    String res = IntStream.range(0, 50)
        .map(el -> r.nextInt(15) + 10)
        .boxed()
        .peek(el -> arr.add(String.valueOf(el)))
        .collect(Collectors.groupingBy(integer -> integer))
        .entrySet()
        .stream()
//        .peek(entry -> System.out.printf("%s :: %s", entry.getKey(), entry.getValue()))
        .min(Comparator.comparingInt(a -> a.getValue().size()))
        .map(entry -> String.format("Number %s %s times", entry.getKey(), entry.getValue().size()))
        .orElse("not found");

    System.out.println(arr.toString());
    System.out.printf("Found result: %s\n", res);
  }
}
