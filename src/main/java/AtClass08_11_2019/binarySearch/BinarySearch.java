package AtClass08_11_2019.binarySearch;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BinarySearch {

  public static void main(String[] args) {
    List<Integer> arr = Stream.generate(() -> (int) (Math.random() * 100 + 1))
        .limit(100)
        .sorted()
        .collect(Collectors.toList());

    System.out.println(arr);
    BinarySearch bs = new BinarySearch();

    int randomN = (int) (Math.random() * 100 + 1);
    System.out.printf("for %d: %s\n", randomN, bs.binarySearchRec(arr, randomN));
    System.out.printf("for %d: %s\n", randomN, bs.binarySearch(arr, randomN));

  }

  private int binarySearch(List<Integer> arr, int key) {
    int l = 0;
    int r = arr.size() - 1;

    while (l < r) {
      int mid = (l + r) / 2;
      if (arr.get(mid) == key) return mid;
      else if (arr.get(mid) < key) {
        l = mid + 1;
      } else {
        r = mid - 1;
      }
    }
    return -1;
  }

  private int binarySearchRec(List<Integer> arr, int key) {
    return recursionBody(arr, key, 0, arr.size() - 1);
  }

  private int recursionBody(List<Integer> arr, int key, int l, int r) {
    if (l >= r) return -1;
    int mid = (l + r) / 2;

    if (arr.get(mid) == key) return mid;
    else if (arr.get(mid) < key) {
      return recursionBody(arr, key, mid + 1, r);
    } else {
      return recursionBody(arr, key, l, mid - 1);
    }
  }
}
