package AtClass08_11_2019.warmup;

public class FilterString {
  public String filter(String str) {
    StringBuilder res = new StringBuilder();
    String temp = str.toLowerCase();
    for (int i = 0; i < str.length(); i++) {
      switch (temp.charAt(i)) {
        case 'a':
        case 'i':
        case 'o':
        case 'e':
        case 'u':
          continue;
        default:
          res.append(str.charAt(i));
      }
    }
    return res.toString();
  }
}
