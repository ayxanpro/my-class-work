package AtClass08_11_2019.warmup;

import java.util.ArrayList;
import java.util.List;

public class MaxPalindrome {

  int count = 0;

  List<Long> generatePrimes(int min, int max) {
    List<Long> primes = generateFirstPrimes(75);
//    System.out.println(primes);
//    System.out.println(primes.size());
    List<Long> res = new ArrayList<>();//generateFirstPrimes(20);
    for (int i = min; i <= max; i += 2) {
      if (check(i, primes)) res.add((long) i);
    }
    System.out.println(count);
    return res;
  }

  boolean check(int n) {
    int l = (int) (Math.sqrt(n) + 1);
    for (int i = 2; i <= l; i++) {
      count++;
      if (n % i == 0) return false;
    }
    return true;
  }

  boolean check(int n, List<Long> res) {
    for (long i : res) {
      count++;
      if (n % i == 0) return false;
    }
    return true;
  }

  public long maxPalindrome() {
    List<Long> primesOnly = generatePrimes((int) 1e4 + 1, (int) 1e5 - 1);
    long max = 1;

    for (int i = primesOnly.size() - 1; i >= 0; i--) {
      for (int j = i - 1; j >= 0; j--) {
        long pro = primesOnly.get(i) * primesOnly.get(j);
        if (isPalindromeNoString(pro)) {
//          System.out.println(primesOnly.get(i));
//          System.out.println(primesOnly.get(j));
          max = Math.max(max, pro);
        }
      }
    }
    return max;
  }

  public boolean isPalindromeNoString(long number) {

    long reversed = 0;
    long temp = number;

    while (temp != 0) {
      reversed = reversed * 10 + temp % 10;
      temp /= 10;
    }
    return number == reversed;
  }

  List<Long> generateFirstPrimes(int n1) {
    ArrayList<Long> res = new ArrayList<>(10000);
    res.add((long) 2);
    for (int i = 3; res.size() < n1; i += 2) {
      boolean flag = true;
      for (int j = 0; j < res.size(); j++) {
        count++;
        if (i % res.get(j) == 0) {
          flag = false;
          break;
        }
      }
      if (flag) res.add((long) i);
    }
    return res;
  }
}
