package AtClass08_11_2019.warmup;

import java.util.ArrayList;
import java.util.List;

public class PrimeMan {
  boolean check(int n) {
    for (int i = 2; i < (int) (Math.sqrt(n)) + 1; i++) {
      if (n % i == 0) return false;
    }
    return true;
  }

  List<Integer> generateFirstPrimes(int count) {
    ArrayList<Integer> res = new ArrayList<>();
    for (int i = 2; res.size() < count; i++) {
      boolean flag = true;
      for(int j=0; j < res.size(); j++){
          if(i % res.get(j) == 0){
            flag = false;
            break;
          }
      }
      if(flag)res.add(i);
    }
    return res;
  }
}
