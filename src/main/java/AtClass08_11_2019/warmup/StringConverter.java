package AtClass08_11_2019.warmup;

public class StringConverter {
  String convert(String str) {
    byte[] letters = str.getBytes();
    for(int i =0; i< str.length(); i++){
      letters[i] ^= 1 << 5;
    }
    return new String(letters);
  }
}
