package AtClass08_11_2019.warmup;


public class WarmUp {


  public boolean isPalindrome(int number) {
    String str = String.valueOf(number);
    for (int i = 0; i < str.length() / 2 + 1; i++) {
      if (str.charAt(i) != str.charAt(str.length() - i - 1)) return false;
    }
    return true;
  }

  public boolean isPalindromeNoString(int number) {

    int reversed = 0;
    int temp = number;

    while (temp != 0){
      reversed = reversed * 10 + temp % 10;
      temp /= 10;
    }
    return number == reversed;
  }

  public boolean isPalindrome(String str) {
    for (int i = 0; i < str.length(); i++) {
      if (str.charAt(i) != str.charAt(str.length() - i - 1)) return false;
    }
    return true;
  }
}
