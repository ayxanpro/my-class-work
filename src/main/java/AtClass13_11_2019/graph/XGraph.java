package AtClass13_11_2019.graph;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class XGraph {
  private int v_count;
  private List<Integer>[] links;

  public XGraph(int v_count) {
    this.v_count = v_count;
    this.links = new ArrayList[v_count];
    for (int i = 0; i < v_count; i++) {
      links[i] = new ArrayList<>();
    }
  }

  public void add(int from, int to) {
    links[from].add(to);
  }

  public void remove(int from, int to) {
    links[from].remove(Integer.valueOf(to));
  }

  public void print_dfs(int start) {
    ArrayDeque<Object> order = new ArrayDeque<>();
    boolean[] visited = new boolean[v_count];
    order.push(start);
    while (!order.isEmpty()) {
      int top = (int) order.pop();
      visited[top] = true;
      System.out.printf("%d ", top);
      for (int i : links[top]) {
        if (!visited[i])
          order.push(i);
      }
    }
  }

  public void print_bfs(int start) {
    ArrayDeque<Object> order = new ArrayDeque<>();
    boolean[] visited = new boolean[v_count];
    order.add(start);
    while (!order.isEmpty()) {
      int top = (int) order.poll();
      visited[top] = true;
      System.out.printf("%d ", top);
      for (int i : links[top]) {
        if (!visited[i])
          order.add(i);
      }
    }
  }

  public void print_dfs_rec(int i) {
    System.out.printf("%d ", i);
    for (int j : links[i]) {
      print_dfs_rec(j);
    }
  }
}
