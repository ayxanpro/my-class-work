package AtClass14_10_2019;

import java.util.Random;

public class MatrixSignSeparetion {
  public static void main(String[] args) {
    int[][] arr = new int[5][5];
    Random r = new Random();
    //Generating random matrix with elements in the range [-10,10]
    System.out.println("Random matrix is: ");
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        arr[i][j] = r.nextInt(20) - 10;
        System.out.printf("%4d ", arr[i][j]);
      }
      System.out.println();
    }
    System.out.println("--------------------------");
    //Printing positive values
    System.out.println("Positive values are: ");
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        if (arr[i][j] > 0)
          System.out.printf("%4d", arr[i][j]);
        else {
          System.out.print("    ");
        }
      }
      System.out.println();
    }
    System.out.println("--------------------------");
    //Printing negative values
    System.out.println("Negative values are: ");
    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr[i].length; j++) {
        if (arr[i][j] < 0)
          System.out.printf("%4d", arr[i][j]);
        else {
          System.out.print("    ");
        }
      }
      System.out.println();
    }
    System.out.println("-----------------");
  }
}
