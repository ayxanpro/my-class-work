package AtClass14_10_2019;

import java.util.Arrays;
import java.util.Random;

public class SplitingRandomArray {
  static int[] filter_negatives(int[] arr) {
    int[] res = new int[arr.length];
    int last = 0;
    for (int i = 0; i < res.length; i++) {
      if (arr[i] < 0) {
        res[last++] = arr[i];
      }
    }
    return Arrays.copyOf(res, last);
  }

  static int[] filter_evens(int[] arr) {
    int[] res = new int[arr.length];
    int last = 0;
    for (int i = 0; i < res.length; i++) {
      if (arr[i] >= 0 && arr[i] % 2 == 0) {
        res[last++] = arr[i];
      }
    }
    return Arrays.copyOf(res, last);
  }

  static int[] filter_odds(int[] arr) {
    int[] res = new int[arr.length];
    int last = 0;
    for (int i = 0; i < res.length; i++) {
      if (arr[i] >= 0 && arr[i] % 2 != 0) {
        res[last++] = arr[i];
      }
    }
    return Arrays.copyOf(res, last);
  }

  static int[] generate(int size) {
    int res[] = new int[size];
    Random r = new Random();
    for (int i = 0; i < size; i++) {
      res[i] = r.nextInt(20) - 10;
    }
    return res;
  }

  public static void main(String[] args) {
    int[] mainArr, negArr, evenArr, oddArr;
    int size = 30;
    //Generating
    mainArr = generate(size);
    //filtering
    negArr = filter_negatives(mainArr);
    evenArr = filter_evens(mainArr);
    oddArr = filter_odds(mainArr);
    //Printing
    System.out.printf("Initial array : %s\n", Arrays.toString(mainArr));

    System.out.printf("negatives : %s\n", Arrays.toString(negArr));

    System.out.printf("evens : %s\n", Arrays.toString(evenArr));

    System.out.printf("odds : %s\n", Arrays.toString(oddArr));
  }
}
