package AtClass14_10_2019;

public class SwapFunction {
  static int swap(int x, int y) {
    return x;
  }

  public static void main(String[] args) {
    int x = 5, y = 7;
    x = swap(y, y = x);
    System.out.println(x + " " + y);
  }
}
