package AtClass15_11_2019.warmup;

import java.util.Arrays;

public class ShiftingNegatives {
  public static void main(String[] args) {
    int[] arr = new int[30];
    for(int i=0; i < arr.length; i++){
      arr[i] = (int)(Math.random() * 20 - 10);
    }

    System.out.println(Arrays.toString(arr));
    shift_arr(arr);
    System.out.println(Arrays.toString(arr));
  }

  public static void shift_arr(int[] arr) {
    int first_neg = -1;
    int curr = 0;

    for (int i = 0; i < arr.length; i++) {
      if (arr[i] > 0) continue;

      if (first_neg == -1) {
        first_neg = i;
        curr = arr[i];
      } else {
        int temp = arr[i];
        arr[i] = curr;
        curr = temp;
      }
    }
    arr[first_neg] = curr;
  }
}
