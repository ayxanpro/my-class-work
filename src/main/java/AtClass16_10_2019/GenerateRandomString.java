package AtClass16_10_2019;

import java.util.Random;
import java.util.Scanner;

public class GenerateRandomString {
  /**
   * @param len
   * @return string of random characters of length len
   */
  static String generate(int len) {
    Random r = new Random();
    String res = "";
    for (int i = 0; i < len; i++) {
      res += (char) ('A' + r.nextInt(26));
    }
    return res;
  }

  public static void main(String[] args) {

    Scanner scanner = new Scanner(System.in);

    //Getting user input
    System.out.println("Please enter the length for the string: ");
    int len = scanner.nextInt();

    //Printing the result of the generate function
    System.out.println(generate(len));
  }
}

