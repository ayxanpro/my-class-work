package AtClass18_10_2019;

public class ComparingStrings {
  public boolean check(String a, String b) {
    if (a == null || b == null) return false;
    if (a.length() != b.length()) return false;
    for (int i = 0; i < a.length(); i++) {
      if (a.charAt(i) != b.charAt(i)) return false;
    }
    return true;
  }

  public static void main(String[] args) {
  }

}
