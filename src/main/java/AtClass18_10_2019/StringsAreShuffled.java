package AtClass18_10_2019;

import java.util.Arrays;

public class StringsAreShuffled {
  public static boolean check(String a, String b) {
    if (a == null || b == null) return false;
    if (a.length() != b.length()) return false;
    String[] a_ = a.split("");
    String[] b_ = b.split("");
    Arrays.sort(a_);
    Arrays.sort(b_);
    return Arrays.equals(a_, b_);
  }

  public static void main(String[] args) {
    String a = "abcd";
    String b = "dcab";
    System.out.println(check(a, b));
  }
}
