package AtClass18_10_2019;

public class StringsAreTheRotated {
  public boolean check(String a, String b) {
    if (a == null || b == null) return false;
    if (a.length() != b.length()) return false;
    for (int i = 0; i < b.length(); i++) {
      if (a.equals(b)) return true;
      b = b.substring(1) + b.charAt(0);
    }
    return false;
  }

  public static boolean check_with_no_loops(String a, String b) {
    if (a == null || b == null) return false;
    if (a.length() != b.length()) return false;
    return a.concat(a).contains(b);
  }

  public static void main(String[] args) {
    String a = "HelloWorld";
    String b = "WorldHello";
    System.out.println(check_with_no_loops(a, b));
  }

}
