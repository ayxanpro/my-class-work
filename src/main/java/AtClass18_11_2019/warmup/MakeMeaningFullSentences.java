package AtClass18_11_2019.warmup;

import java.util.*;
import java.util.stream.Collectors;

public class MakeMeaningFullSentences {

  public static List<String> printBody(Map<String, List<String>> subject_verb,
                                       Map<String, List<String>> verb_object) {
    ArrayList<String> res = new ArrayList<>();
    for (String sub : subject_verb.keySet()) {
      List<String> verbs = subject_verb.get(sub);
      for (String verb : verbs) {
        List<String> objects = verb_object.get(verb);
        for (String obj : objects) {
          res.add(String.format("%s %s %s", sub, verb, obj));
        }
      }
    }
    return res;
  }

  public static List<String> printRec(Map<String, List<String>> subject_verb,
                                      Map<String, List<String>> verb_object) {
    return subject_verb.entrySet().stream()
        .flatMap(subject -> subject.getValue().stream()
            .flatMap(verb ->
                verb_object.get(verb)
                    .stream()
                    .map(object ->
                        String.format("%s %s %s", subject.getKey(), verb, object)
                    )
            )
        )
        .collect(Collectors.toList());
  }

  public static void main(String[] args) {

    Map<String, List<String>> assoc_subj_verb = map(
        of("Noel", list("wrote", "chased", "slept on")),
        of("The cat", list("meowed at", "chased", "slept on")),
        of("The dog", list("barked at", "chased", "slept on")));

    Map<String, List<String>> assoc_verb_obj = map(
        of("wrote", list("the book", "the letter", "the code")),
        of("chased", list("the ball", "the dog", "the cat")),
        of("slept on", list("the bed", "the mat", "the train")),
        of("meowed at", list("Noel", "the door", "the food cupboard")),
        of("barked at", list("the postman", "the car", "the cat")));

    List<String> ress = printRec(assoc_subj_verb, assoc_verb_obj);//printBody(assoc_subj_verb, assoc_verb_obj);

    ress.forEach(System.out::println);
  }

  private static Map<String, List<String>> map(Map.Entry<String, List<String>>... pairs) {
    HashMap<String, List<String>> res = new HashMap<>();
    for (Map.Entry<String, List<String>> pair : pairs) {
      res.put(pair.getKey(), pair.getValue());
    }
    return res;
  }

  private static Map.Entry<String, List<String>> of(String key, List<String> value) {
    return new AbstractMap.SimpleEntry<>(key, value);
  }

  private static List<String> list(String... vals) {
    return new ArrayList<>(Arrays.asList(vals));
  }
}
