package AtClass18_11_2019.warmup;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class MakeSentences {
  public static void printAllVariants(List<String> subjects, List<String> verbs, List<String> objects) {
    StringJoiner sj = new StringJoiner("\n");
    for (String subject : subjects) {
      for (String verb : verbs) {
        for (String object : objects) {
          sj.add(String.format("%s %s %s", subject, verb, object));
        }
      }
    }
    System.out.println(sj.toString());
  }

  public static List<String> printAllVariantsRecur(List<String> subjects,
                                                   List<String> verbs,
                                                   List<String> objects) {
    return printBody(subjects, verbs, objects, 0, 0, 0, new ArrayList<String>());
  }

  public static List<String> printBody(List<String> subjects,
                                       List<String> verbs,
                                       List<String> objects, int i, int j, int k, List<String> res) {
    if (i == subjects.size()) {
      return res;
    }
    if (j == verbs.size()) {
      printBody(subjects, verbs, objects, i + 1, 0, 0, res);
      return res;
    }
    if (k == objects.size()) {
      printBody(subjects, verbs, objects, i, j + 1, 0, res);
      return res;
    }
    res.add(String.format("%s %s %s\n", subjects.get(i), verbs.get(j), objects.get(k)));
    return printBody(subjects, verbs, objects, i, j, k + 1, res);
  }

  public static List<String> printStreams(List<String> subjects,
                                          List<String> verbs,
                                          List<String> objects) {
    return subjects.stream()
        .flatMap(subject ->
            verbs.stream()
                .flatMap(verb ->
                    objects.stream()
                        .map(object -> String.format("%s %s %s", subject, verb, object))))
        .collect(Collectors.toList());
  }

  public static void main(String[] args) {
    ArrayList<String> subjects = new ArrayList<String>() {{
      add("Noel");
      add("The cat");
      add("The dog");
    }};

    ArrayList<String> verbs = new ArrayList<String>() {{
      add("wrote");
      add("chased");
      add("slept on");
    }};
    ArrayList<String> objects = new ArrayList<String>() {{
      add("the book");
      add("the ball");
      add("the bed");
    }};
//    printAllVariants(subjects, verbs, objects);
    System.out.println(printStreams(subjects, verbs, objects));
  }

}
