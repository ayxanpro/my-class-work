package AtClass18_11_2019.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.StringJoiner;

public class LoginServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter writer = resp.getWriter();
    Map<String, String[]> parameterMap = req.getParameterMap();
    String[] xs = parameterMap.get("x");
    StringJoiner sj = new StringJoiner("\n");
    for (String x : xs) {
      sj.add(x);
    }
    writer.println(sj.toString());
  }
}
