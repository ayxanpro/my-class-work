package AtClass18_11_2019.web;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class WebApp {
  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);

    ServletContextHandler handler = new ServletContextHandler();
//    handler.addServlet(new ServletHolder(new LoginServlet()),"/login");
//    handler.addServlet(new ServletHolder(new calcServlet()),"/calc");
//    handler.addServlet(new ServletHolder(new HelloServlet()),"/hello/*");

    server.setHandler(handler);
    server.start();
    server.join();
  }

}
