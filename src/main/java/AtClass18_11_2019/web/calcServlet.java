package AtClass18_11_2019.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class calcServlet extends HttpServlet {

  private String calc(String x_str, String y_str, String op) {
    try {
      double x = Double.parseDouble(x_str);
      double y = Double.parseDouble(y_str);
      String res;
      switch (op) {
        case "plus":
          res = String.format("%s + %s = %s", x, y, x + y);
          break;
        case "minus":
          res = String.format("%s - %s = %s", x, y, x - y);
          break;
        case "mult":
          res = String.format("%s * %s = %s", x, y, x * y);
          break;
        case "div":
          if (y != 0) res = String.format("%s / %s = %s", x, y, x / y);
          else throw new IllegalArgumentException("Division by zero!!!!!!");
          break;
        default:
          throw new IllegalArgumentException("Unknown command");
      }
      return String.format("<h1>%s</h1>", res);

    } catch (NumberFormatException e) {
      return "<h1 style='color:red'>Can't convert numbers</h1>";
    } catch (Exception e) {
      return "<h1 style='color:red; font-size:64px'>Exception: " + e.getMessage() + "</h1>";
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter writer = resp.getWriter();
    writer.println("<h1>Calc:Servlet</h1>");

    String x = req.getParameter("x");
    String y = req.getParameter("y");
    String op = req.getParameter("op");

    String res = calc(x, y, op);
    writer.printf("<br>" +
        "%s", res);

    writer.close();
  }
}
