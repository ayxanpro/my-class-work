package AtClass21_10_2019.Figures;


public class Circle extends Figure {
  Point center;
  int radius;

  public Circle(Point center, int radius) {
    this.center = center;
    this.radius = radius;
  }

  public Circle() {
    this.center = Point.randomPoint(100, 100);
    this.radius = Point.random(10);
  }

  @Override
  public double calcArea() {
    return Math.PI * radius * radius;
  }

  public String toString() {
    return String.format("Circle at point %s and with radius %d", center, radius);
  }

  public boolean equals(Object that) {
    if (this == that) return true;
    if (!(that instanceof Circle)) return false;
    Circle b = (Circle) that;

    if (b.center.equals(this.center)
        && b.radius == this.radius) return true;
    return false;
  }


}
