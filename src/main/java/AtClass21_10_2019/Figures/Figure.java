package AtClass21_10_2019.Figures;

public abstract class Figure {
  public abstract double calcArea();
}
