package AtClass21_10_2019.Figures;

import java.util.ArrayList;
import java.util.Random;

public class Main {
  public static void main(String[] args) {
    ArrayList<Figure> figures = new ArrayList<>();
    Random r = new Random();

    for (int i = 0; i < 10; i++) {
      int fig = r.nextInt(4);
      switch (fig) {
        case 0:
          figures.add(new Circle());
          break;
        case 1:
          figures.add(new Triangle());
          break;
        case 2:
          figures.add(new Rectangle());
          break;
        case 3:
          figures.add(new Square());
          break;
      }
    }

    double total = 0;
    for (Figure fig : figures) {
      System.out.println(fig);
      System.out.println("Area is: " + fig.calcArea());
      total += fig.calcArea();
    }
    System.out.println("Total area: " + total);
  }
}
