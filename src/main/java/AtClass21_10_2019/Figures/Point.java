package AtClass21_10_2019.Figures;

import java.util.Random;

public class Point {
  int x, y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public static Point randomPoint(int x, int y) {
    return new Point(random(x), random(y));
  }

  public static int random(int i) {
    return (new Random().nextInt(i));
  }

  @Override
  public String toString() {
    return String.format("(%d, %d)", x, y);
  }

  public boolean equals(Object that) {
    if (this == that) return true;
    if (!(that instanceof Point)) return false;
    Point b = (Point) that;

    if (b.x == this.x && b.y == this.y) return true;
    return false;
  }
}
