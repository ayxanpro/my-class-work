package AtClass21_10_2019.Figures;

public class Rectangle extends Figure {
  Point p1, p2;

  public Rectangle(Point p1, Point p2) {
    this.p1 = p1;
    this.p2 = p2;
  }

  public Rectangle() {
    this.p1 = Point.randomPoint(100, 100);
    this.p2 = Point.randomPoint(100, 100);
  }

  @Override
  public double calcArea() {
    return Math.abs(p1.x - p2.x) * Math.abs(p2.y - p1.y);
  }

  public String toString() {
    return String.format("Rectangle at points: %s and %s", p1, p2);
  }

  public boolean equals(Object that) {
    if (this == that) return true;
    if (!(that instanceof Rectangle)) return false;
    Rectangle b = (Rectangle) that;

    if (b.p1.equals(this.p1)
        && b.p2.equals(this.p2)) return true;
    return false;
  }
}
