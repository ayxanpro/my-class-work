package AtClass21_10_2019.Figures;

public class Square extends Rectangle {

  public Square(Point p1, Point p2) {
    super(p1, p2);
  }

  public Square() {
    p1 = Point.randomPoint(100, 100);
    int x2 = Point.random(100);
    p2 = new Point(x2, p1.y + (x2 - p1.x));
  }

  public String toString() {
    return String.format("Square at points: %s and %s", p1, p2);
  }
}
