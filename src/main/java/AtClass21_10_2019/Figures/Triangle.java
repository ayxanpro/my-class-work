package AtClass21_10_2019.Figures;

public class Triangle extends Figure {
  Point p1, p2, p3;

  public Triangle(Point p1, Point p2, Point p3) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
  }

  public Triangle() {
    this.p1 = Point.randomPoint(100, 100);
    this.p2 = Point.randomPoint(100, 100);
    this.p3 = Point.randomPoint(100, 100);
  }

  @Override
  public double calcArea() {
    double det = p1.x * p2.y - p2.x * p1.y
        + p2.x * p3.y - p3.x * p2.y
        + p3.x * p1.y - p1.x * p3.y;
    return Math.abs(det / 2);
  }

  public String toString() {
    return String.format("Triangle at points: %s, %s and %s", p1, p2, p3);
  }

  public boolean equals(Object that) {
    if (this == that) return true;
    if (!(that instanceof Triangle)) return false;
    Triangle b = (Triangle) that;

    if (b.p1.equals(this.p1)
        && b.p2.equals(this.p2)
        && b.p3.equals(this.p3)) return true;
    return false;
  }
}
