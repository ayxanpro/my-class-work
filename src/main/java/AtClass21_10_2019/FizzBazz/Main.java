package AtClass21_10_2019.FizzBazz;

import java.util.Arrays;
import java.util.Random;

public class Main {
  public static void main(String[] args) {

    final int length = 20;
    int[] numbers = new int[20];
    Random r = new Random();

    for (int i = 0; i < length; i++) {
      numbers[i] = r.nextInt(10);
    }

    System.out.println("Random array: " + Arrays.toString(numbers));

    for (int el : numbers) {
      if (el % 6 == 0) System.out.println("fizzbuzz");
      else if (el % 3 == 0) System.out.println("buzz");
      else if (el % 2 == 0) System.out.println("fizz");
      else System.out.println();
    }
  }
}
