package AtClass22_11_2019;

import java.util.Scanner;

public class WarmUp {

  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter the n: ");
    int n = scanner.nextInt();
    System.out.printf("%d %d", solution1(n), solution2(n));
  }

  public static int solution1(int n) {
    int curr = 0;
    int last = 0;
    for (int i = 0; curr < n; i++) {
      int k = i * 3;
//      int k = i + i + i;
      if (k % 2 != 0) {
        last = k;
        curr++;
      }
    }
    return last;
  }

  public static int solution2(int n) {
    return 3 + 6 * (n - 1);
  }

//  public static int solution3(int n) {
//    IntStream.range(1, Integer.MAX_VALUE).parallel()
//        .map(i -> i * 3)
//        .filter(i -> i % 2 == 0);
//  }

}
