package AtClass22_11_2019.db;

public interface Auth {
  int login(String username,String pass);
  void register(String username,String pass);
}
