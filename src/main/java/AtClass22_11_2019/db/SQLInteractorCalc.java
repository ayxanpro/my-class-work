package AtClass22_11_2019.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringJoiner;

public class SQLInteractorCalc {
  final String SQLS = "SELECT * FROM calc_history WHERE user_id=?";
  final String SQLI = "INSERT INTO calc_history (result,x,y,operation,user_id) values (?,?,?,?,?)";

  public void insert(String res, String x, String y, String op, int id) {
    try (PreparedStatement stmt_insert = DbConnection.getConnection().prepareStatement(SQLI)) {

      stmt_insert.setString(1, res);
      stmt_insert.setString(2, x);
      stmt_insert.setString(3, y);
      stmt_insert.setString(4, op);
      stmt_insert.setInt(5, id);
      stmt_insert.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public String getAll(int id) {
    try (PreparedStatement statement = DbConnection.getConnection().prepareStatement(SQLS)) {
      StringJoiner sj = new StringJoiner("\n");
      statement.setInt(1,id);
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        String line = String.format("<<%d>>: %s", resultSet.getInt("id")
            , resultSet.getString("result"));
        sj.add(line);
      }
      return sj.toString();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "empty";
  }
}
