package AtClass22_11_2019.web;

import AtClass22_11_2019.db.SQLInteractorCalc;
import AtClass22_11_2019.db.UserAuth;
import AtClass22_11_2019.web.servlets.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class WebApp {
  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);
    SQLInteractorCalc sqlInteractor = new SQLInteractorCalc();
    ServletContextHandler handler = new ServletContextHandler();
    handler.addServlet(new ServletHolder(new calcServlet(sqlInteractor)), "/calc");
    UserAuth auth = new UserAuth();

    handler.addServlet(new ServletHolder(new LoginServlet(auth)), "/login");
    handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
    handler.addServlet(new ServletHolder(new RegisterServlet(auth)), "/register");
    handler.addServlet(new ServletHolder(new CalcHistoryServlet(sqlInteractor)), "/history");

    handler.addServlet(new ServletHolder(new StaticLoader()), "/static/*");
    server.setHandler(handler);
    server.start();
    server.join();
  }

}
