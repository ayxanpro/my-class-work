package AtClass22_11_2019.web.servlets;


import AtClass22_11_2019.db.SQLInteractorCalc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CalcHistoryServlet extends HttpServlet {
  private final SQLInteractorCalc sql;

  public CalcHistoryServlet(SQLInteractorCalc sql) {
    this.sql = sql;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getCookies()[0].getValue());
    String all = sql.getAll(id);
    String logoutlink = "<br><a href='/logout'>logout</a>";
    resp.getWriter().println(all + logoutlink);
  }
}
