package AtClass22_11_2019.web.servlets;

import AtClass22_11_2019.db.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LoginServlet extends HttpServlet {
  private final Auth auth;

  public LoginServlet(Auth auth) {
    this.auth = auth;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Files.copy(Paths.get("./files/default.html"), resp.getOutputStream());
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String username = req.getParameter("username");
    String password = req.getParameter("password");
    int id = auth.login(username, password);
    System.out.println(id);
    if (id != -1) {
      Cookie cookie = new Cookie("id", String.valueOf(id));
      resp.addCookie(cookie);
      resp.sendRedirect("/calc");
    } else {
      resp.sendRedirect("/login");
    }
  }
}
