package AtClass23_10_2019.PrintOnlyOdds;

import java.util.ArrayList;
import java.util.Random;

public class Main {
  public static void main(String[] args) {
    Random r = new Random();
    ArrayList<Integer> list = new ArrayList<>();

    for (int i = 0; i < 100; i++) {
      list.add(r.nextInt(100));
    }

    ArrayList<Integer> odds = new ArrayList<>();
    ArrayList<Integer> evens = new ArrayList<>();

    for (Integer i : list) {
      if (i % 2 != 0) odds.add(i);
      else evens.add(i);
    }

    System.out.println("My array: " + list);
    System.out.println("Odds:" + odds);
    System.out.println("Evens:" + evens);

    System.out.println();
  }
}
