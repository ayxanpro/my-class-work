package AtClass23_10_2019.PrintOnlyOdds;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class UsingFilters {
  public static void main(String[] args) {
    Random r = new Random();
    ArrayList<Integer> list = new ArrayList<>();

    for (int i = 0; i < 100; i++) {
      list.add(r.nextInt(100));
    }

    ArrayList<Integer> odds = (ArrayList<Integer>) list.stream()
        .filter(i -> (i % 2 != 0))
        .collect(Collectors.toList());
    ArrayList<Integer> evens = (ArrayList<Integer>) list.stream()
        .filter(i -> (i % 2 == 0))
        .collect(Collectors.toList());

    System.out.println("Whole array: " + list);
    System.out.println("Odds:" + odds);
    System.out.println("Evens:" + evens);

    System.out.println();
  }
}
