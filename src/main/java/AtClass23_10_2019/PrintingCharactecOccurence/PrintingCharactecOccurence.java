package AtClass23_10_2019.PrintingCharactecOccurence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class PrintingCharactecOccurence {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    String input = scanner.nextLine();
    input = input.toLowerCase();
    HashMap<Character, ArrayList<Integer>> res = new HashMap<>();
    for (int i = 0; i < input.length(); i++) {
      Character c = input.charAt(i);

      if (!(c >= 'a' && c <= 'z')) continue;

      if (res.containsKey(c)) {
        res.get(c).add(i + 1);
      } else {
        ArrayList<Integer> temp = new ArrayList<>();
        temp.add(i + 1);
        res.put(c, temp);
      }
    }
    for (Character c : res.keySet()) {
      System.out.printf("<'%s' : %d : %s>\n", c, res.get(c).size(), res.get(c).toString());
    }
    Arrays.stream(Arrays.stream(input.toLowerCase().split(""))
        .map(el -> new int[]{(int) el.charAt(0)})
        .filter(el -> (el[0] >= 'a' && el[0] <= 'z'))
        .reduce(new int[26], (array, item) -> {
          array[item[0] - 'a']++;
          return array;
        })).reduce(0, (index, el) -> {
      if (el > 0)
        System.out.printf("%s : %d\n", (char) (index + 'a') + "", el);
      return index + 1;
    });
    //System.out.println(Arrays.toString(reduced));
    //.reduce(new int[26], (array, c) -> array[c - 'a']++);
  }
}
