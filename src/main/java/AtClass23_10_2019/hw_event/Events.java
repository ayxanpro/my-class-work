package AtClass23_10_2019.hw_event;

import java.util.Iterator;

public class Events implements Iterable<Event> {

  private final Event[] data;

  public Events(int number) {
    this.data = new Event[number];
    data[0] = new Event(1942, "World War II");
    data[1] = new Event(1991, "Independence");
  }

  @Override
  public Iterator<Event> iterator() {
    return new Iterator<Event>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        return index < data.length;
      }

      @Override
      public Event next() {
        return data[index++];
      }
    };
  }

  public static Events create() {
    return new Events(2);
  }

  public boolean find(int year) {
    boolean found = false;
    for (Event e : data) {
      if (e.checkYear(year)) return true;
    }
    return found;
  }

  public String get(int year) {
    for (Event e : data) {
      if (e.checkYear(year)) return e.toString();
    }
    return "";
  }

}
