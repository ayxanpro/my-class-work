package AtClass23_10_2019.hw_event;

public class EventsApp {
  public static void main(String[] args) {
    Events events = Events.create();
    for (Event e : events) {
      System.out.println(e);
    }
  }
}
