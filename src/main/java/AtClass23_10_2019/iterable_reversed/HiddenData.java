package AtClass23_10_2019.iterable_reversed;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public final class HiddenData implements Iterable<String> {

  private final List<String> data = Arrays.asList(
      "Winter", "Summer", "Spring", "Autumn"
  );

  @Override
  public Iterator<String> iterator() {
    return data.iterator();
  }

  private final int[] data2 = new int[]{1, 2, 3, 4};

  public Iterator<Integer> iterator_data2() {

    Iterator<Integer> myIterator = new Iterator<Integer>() {
      int current = data2.length - 1;

      @Override
      public boolean hasNext() {
        return current > -1;
      }

      @Override
      public Integer next() {
        return data2[current--];
      }
    };

    return myIterator;
  }

}
