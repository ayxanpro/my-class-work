package AtClass23_10_2019.random_iterator;

public class Main {
  public static void main(String[] args) {
    MonthContainer months = new MonthContainer();
    for (String month : months) {
      System.out.println(month);
    }
  }
}
