package AtClass23_10_2019.random_iterator;

import java.util.*;

public class MonthContainer implements Iterable<String> {
  private final List<Months> data = Arrays.asList(Months.values());

  @Override
  public Iterator<String> iterator() {
    return new Iterator<String>() {
      int curr = 0;
      Set<Integer> used = new HashSet<>();

      @Override
      public boolean hasNext() {
        return used.size() < data.size();
      }

      @Override
      public String next() {
        do {
          curr = (int) (Math.random() * data.size());
        } while (used.contains(curr));
        used.add(curr);
        return data.get(curr).name();
      }
    };
  }
}
