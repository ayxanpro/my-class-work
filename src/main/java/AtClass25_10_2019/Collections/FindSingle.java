package AtClass25_10_2019.Collections;

import java.util.Arrays;

public class FindSingle {
  static int findPair(int[] arr) {
    return Arrays.stream(arr).reduce(0, (sum, el) -> sum + el);
  }

  public static void main(String[] args) {
    int[] arr = new int[]{1, 2, 3, 4, 5, -5, -3, -2, -1};
    System.out.println(findPair(arr));
  }
}
