package AtClass25_10_2019.Collections;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class RandomSet {
  public static void main(String[] args) {
    Set<Integer> my_set = new HashSet<>();

    Random r = new Random();
    while (my_set.size() < 20) {
      my_set.add(r.nextInt(21) - 10);
    }
    Set<Integer> sorted = new TreeSet<>(my_set);
    System.out.println("Random set" + my_set);
    System.out.println("Sorted set" + sorted);
  }
}
