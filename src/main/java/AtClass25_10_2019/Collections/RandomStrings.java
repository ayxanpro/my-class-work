package AtClass25_10_2019.Collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class RandomStrings {
  public static String generateString(int min, int max) {
    Random r = new Random();
    StringBuilder res = new StringBuilder("");
    int len = r.nextInt(max - min + 1) + min;
    int i = 0;
    while (i < len) {
      res.append((char) (r.nextInt(26) + 'a'));
      i++;
    }
    return res.toString();
  }

  public static void main(String[] args) {
    Map<Integer, String> map = new HashMap<>();
    final int min = 10, max = 30;
    int i = 0;
    while (i < 20) {
      String random = generateString(min, max);
      map.put(i + 1, random);
      i++;
    }

    String maxString = map.get(1), minString = map.get(1);

    for (Map.Entry<Integer, String> el : map.entrySet()) {
      if (maxString.length() < el.getValue().length()) {
        maxString = el.getValue();
      }
      if (minString.length() > el.getValue().length()) {
        minString = el.getValue();
      }
    }
    System.out.println("Whole Map" + map);
    System.out.printf("Max String %s, length: %d\n", maxString, maxString.length());
    System.out.printf("Min String %s, length: %d\n", minString, minString.length());
  }
}
