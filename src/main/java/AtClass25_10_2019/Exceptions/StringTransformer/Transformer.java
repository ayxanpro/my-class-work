package AtClass25_10_2019.Exceptions.StringTransformer;

public class Transformer {
  static String transform(String origin) {
    try {
      int res = Integer.parseInt(origin);
    } catch (NumberFormatException ex) {
      return "<wrong>";
    }
    return origin;
  }

  public static void main(String[] args) {
    System.out.println(transform("123"));
    System.out.println(transform("123ads"));
    System.out.println(transform("asd"));
    System.out.println(transform("3423"));
  }
}
