package AtClass25_11_2019.db;

import java.util.Optional;

public interface Auth {
  Optional<Integer> login(String username, String pass);
  void register(String username, String pass);
}
