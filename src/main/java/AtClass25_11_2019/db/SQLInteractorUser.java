package AtClass25_11_2019.db;

import org.omg.CORBA.INTERNAL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.StringJoiner;

public class SQLInteractorUser {
  final String SQLS = "SELECT * FROM users";
  final String SQLI = "INSERT INTO users (name,password) values (?,?)";
  final String LOGIN = "SELECT id,name,password FROM users WHERE name=? and password=?";

  public void insert(String name, String pass) {
    try (PreparedStatement stmt_insert = DbConnection.getConnection().prepareStatement(SQLI)) {
//      System.out.println(name + " " + pass);
      stmt_insert.setString(1, name);
      stmt_insert.setString(2, pass);
      stmt_insert.executeUpdate();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Optional<Integer> log(String name, String pass) {
    try (PreparedStatement preparedStatement = DbConnection.getConnection().prepareStatement(LOGIN)) {
//      System.out.println(name + " " + pass);
      preparedStatement.setString(1, name);
      preparedStatement.setString(2, pass);
      ResultSet resultSet = preparedStatement.executeQuery();

      while (resultSet.next()) {
        int id = resultSet.getInt("id");
        return Optional.of(id);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return Optional.empty();
  }

  public String getAll() {
    try (PreparedStatement statement = DbConnection.getConnection().prepareStatement(SQLS)) {
      StringJoiner sj = new StringJoiner("\n");
      ResultSet resultSet = statement.executeQuery();
      while (resultSet.next()) {
        String line = String.format("<<%d>>: %s", resultSet.getInt("id")
            , resultSet.getString("name"));
        sj.add(line);
      }
      return sj.toString();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "empty";
  }
}
