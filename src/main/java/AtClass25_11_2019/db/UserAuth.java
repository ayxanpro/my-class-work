package AtClass25_11_2019.db;

import java.util.Optional;

public class UserAuth implements Auth {
  private final SQLInteractorUser sql;

  public UserAuth() {
    this.sql = new SQLInteractorUser();
  }

  @Override
  public Optional<Integer> login(String username, String pass) {
    return sql.log(username, pass);
  }

  @Override
  public void register(String username, String pass) {
    sql.insert(username, pass);
  }
}
