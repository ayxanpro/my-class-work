package AtClass25_11_2019.warmup;

import java.util.Stack;

public class BigSum {
  private int digitAt(String s, int i) {
    return i < 0 ? 0 :s.charAt(i) - '0';
  }

  public String Ssum(String n1, String n2) {
    Stack<Character> res = new Stack<>();
    int rem = 0;

    int i = n1.length() - 1;
    int j = n2.length() - 1;

    for (; i >= 0 || j >= 0; ) {
      int temp = rem + digitAt(n1,i--) + digitAt(n2,j--);
      rem = temp / 10;
      res.push((char) (temp % 10 + '0'));
    }
    if (rem != 0) res.push((char) (rem + '0'));

    StringBuilder finalRes = new StringBuilder();
    while (!res.isEmpty()) {
      finalRes.append(res.pop());
    }
    return finalRes.toString();
  }
}
