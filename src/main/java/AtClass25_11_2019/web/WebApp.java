package AtClass25_11_2019.web;

import AtClass25_11_2019.db.SQLInteractorCalc;
import AtClass25_11_2019.db.UserAuth;
import AtClass25_11_2019.web.filters.CalcParam;
import AtClass25_11_2019.web.filters.LoginFilter;
import AtClass25_11_2019.web.filters.LoginParamterFilter;
import AtClass25_11_2019.web.filters.RegisterParamFilter;
import AtClass25_11_2019.web.servlets.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class WebApp {
  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);
    SQLInteractorCalc sqlInteractor = new SQLInteractorCalc();
    ServletContextHandler handler = new ServletContextHandler();
    UserAuth auth = new UserAuth();

    TemplateEngine freemarker = new TemplateEngine("./files");


    handler.addServlet(new ServletHolder(new LoginServlet(auth)), "/login");
    handler.addServlet(new ServletHolder(new LogoutServlet()), "/logout");
    handler.addServlet(new ServletHolder(new calcServlet(sqlInteractor)), "/calc");
    handler.addServlet(new ServletHolder(new RegisterServlet(auth)), "/register");
    handler.addServlet(new ServletHolder(new CalcHistoryServlet(sqlInteractor, freemarker)), "/history");

    handler.addFilter(LoginParamterFilter.class, "/login", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
    handler.addFilter(RegisterParamFilter.class, "/register", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
    handler.addFilter(LoginFilter.class, "/calc/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
    handler.addFilter(LoginFilter.class, "/history/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
    handler.addFilter(CalcParam.class, "/calc/*", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));

    handler.addServlet(new ServletHolder(new StaticLoader()), "/static/*");
    server.setHandler(handler);
    server.start();
    server.join();
  }

}
