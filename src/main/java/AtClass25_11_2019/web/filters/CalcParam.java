package AtClass25_11_2019.web.filters;

import javax.servlet.*;
import java.io.IOException;

public class CalcParam implements Filter {
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    String x = request.getParameter("x");
    String y = request.getParameter("y");
    String op = request.getParameter("op");
    if (request.getParameter("calc") != null) {
      chain.doFilter(request, response);
      return;
    }
    if (x != null && y != null && op != null &&
        x.length() > 0 && y.length() > 0 && op.length() > 0) {
      try {
        Double.parseDouble(x);
        Double.parseDouble(y);
        chain.doFilter(request, response);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public void destroy() {

  }
}
