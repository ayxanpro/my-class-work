package AtClass25_11_2019.web.filters;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    Cookie[] cookies = req.getCookies();
    if (cookies != null && cookies.length > 0) {
      for (Cookie c : cookies) {
        if (c.getName().equalsIgnoreCase("%calc.id%")) {
          chain.doFilter(request, response);
          return;
        }
      }
    }
    HttpServletResponse res = (HttpServletResponse) response;
    res.setStatus(200);
    res.sendRedirect("/login");
  }

  @Override
  public void destroy() {

  }
}
