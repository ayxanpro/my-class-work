package AtClass25_11_2019.web.filters;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class LoginParamterFilter implements Filter {
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {

  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    if (req.getMethod().equalsIgnoreCase("POST")) {
      Map<String, String[]> params = request.getParameterMap();
      if (params.containsKey("username") && params.get("username").length > 0 &&
          params.containsKey("password") && params.get("password").length > 0) {
        chain.doFilter(request, response);
      } else {
          HttpServletResponse res = (HttpServletResponse) response;
          res.sendRedirect("/login");
      }
    } else {
      chain.doFilter(request, response);
    }
  }


  @Override
  public void destroy() {

  }
}
