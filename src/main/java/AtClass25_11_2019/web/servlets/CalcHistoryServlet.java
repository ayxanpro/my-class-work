package AtClass25_11_2019.web.servlets;


import AtClass25_11_2019.db.SQLInteractorCalc;
import AtClass25_11_2019.web.TemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class CalcHistoryServlet extends HttpServlet {
  private final SQLInteractorCalc sql;
  private final TemplateEngine marker;

  public CalcHistoryServlet(SQLInteractorCalc sql, TemplateEngine freemarker) {
    this.sql = sql;
    this.marker = freemarker;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getCookies()[0].getValue());
    String all = sql.getAll(id);
    String logoutLink = "<br><a href='/logout'>logout</a>";
    marker.render("templates/historyPage.html", new HashMap<String, Object>() {{
      put("history", all);
      put("logout", logoutLink);
    }}, resp);
  }
}
