package AtClass25_11_2019.web.servlets;

import AtClass25_11_2019.db.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class LoginServlet extends HttpServlet {
  private final Auth auth;

  public LoginServlet(Auth auth) {
    this.auth = auth;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Files.copy(Paths.get("./files/templates/default.html"), resp.getOutputStream());
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String username = req.getParameter("username");
    String password = req.getParameter("password");
    Optional<Integer> idO = auth.login(username, password);
    if (idO.isPresent()) {
      Integer id = idO.get();
      Cookie cookie = new Cookie("%calc.id%", String.valueOf(id));
      cookie.setPath("/");
      resp.addCookie(cookie);
      resp.sendRedirect("/calc?calc=false");
    } else {
      resp.sendRedirect("/login");
    }
  }
}
