package AtClass25_11_2019.web.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {
//  private final Auth auth;
//
//  public LogoutServlet(Auth auth) {
//    this.auth = auth;
//  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Cookie[] cookies = req.getCookies();

    for (Cookie cookie : cookies) {
      cookie.setMaxAge(0);
      resp.addCookie(cookie);
    }
    resp.sendRedirect("/login");
//    Files.copy(Paths.get("./files/default.html"), resp.getOutputStream());
  }

}
