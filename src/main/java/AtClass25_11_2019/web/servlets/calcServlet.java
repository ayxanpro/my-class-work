package AtClass25_11_2019.web.servlets;

import AtClass25_11_2019.db.SQLInteractorCalc;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class calcServlet extends HttpServlet {
  private final SQLInteractorCalc sql;

  public calcServlet(SQLInteractorCalc sqlInteractor) {
    this.sql = sqlInteractor;
  }

  private String calc(String x_str, String y_str, String op) {
    try {
      double x = Double.parseDouble(x_str);
      double y = Double.parseDouble(y_str);
      String res;
      switch (op) {
        case "plus":
          res = String.format("%s + %s = %s", x, y, x + y);
          break;
        case "minus":
          res = String.format("%s - %s = %s", x, y, x - y);
          break;
        case "mult":
          res = String.format("%s * %s = %s", x, y, x * y);
          break;
        case "div":
          if (y != 0) res = String.format("%s / %s = %s", x, y, x / y);
          else throw new IllegalArgumentException("Division by zero!!!!!!");
          break;
        default:
          throw new IllegalArgumentException("Unknown command");
      }
      return String.format("%s", res);

    } catch (NumberFormatException e) {
      return "<h1 style='color:red'>Can't convert numbers</h1>";
    } catch (Exception e) {
      return "<h1 style='color:red;'>Exception: " + e.getMessage() + "</h1>";
    }
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter writer = resp.getWriter();
    writer.println("<h1>Calc:Servlet</h1>");
    Cookie[] cookies = req.getCookies();
    int id = Integer.parseInt(cookies[0].getValue());

    String x = req.getParameter("x");
    String y = req.getParameter("y");
    String op = req.getParameter("op");
    String res = null;
      res = calc(x, y, op);
    if(req.getParameter("calc") == null)
      sql.insert(res, x, y, op, id);
    String logoutlink = "<br><a href='/logout'>logout</a>";
    String history = "<br><a href='/history'>History</a>";
    writer.printf("<br>" +
        "<h1>%s</h1>%s%s", res, logoutlink, history);
    writer.close();
  }
}
