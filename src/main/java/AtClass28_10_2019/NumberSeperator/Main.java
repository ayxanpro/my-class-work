package AtClass28_10_2019.NumberSeperator;

public class Main {
  public static void main(String[] args) {
    Solution s = new Solution();
    System.out.println(s.add(null));
    System.out.println(s.oneLineSolution("1 2 3"));
    System.out.println(s.oneLineSolution("1 2a 3"));
    System.out.println(s.oneLineSolution("asd xcv"));
  }
}
