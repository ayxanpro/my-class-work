package AtClass28_10_2019.NumberSeperator;

import java.util.Arrays;

public class Solution {
  int add(String str) {
    if (str == null) return 0;
    String[] parts = str.split(" ");
    int res = 0;
    for (String part : parts) {
      try {
        res += Integer.parseInt(part);
      } catch (Exception e) {
        //Ignoring
      }
    }
    return res;
  }

  int oneLineSolution(String str) {
    return str != null ? Integer.parseInt(Arrays.stream(str.split(" ")).reduce("0", (sum, el) -> {
      try {
        return String.format("%d", Integer.parseInt(sum) + Integer.parseInt(el));
      } catch (Exception e) {
        return sum;
      }
    })) : 0;
  }
}
