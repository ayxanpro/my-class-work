package AtClass28_10_2019.RandomSequenceUsingStreams;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
  public static void main(String[] args) {
    Random r = new Random();

    String result = Stream.generate(() -> r.nextInt(41))
        .limit(10)
        //.map(el -> el * 2)
        //.filter(el -> el > 0)
        .collect(Collectors.toList())
        .stream()
        .collect(Collectors.groupingBy(el -> el % 2))
        .entrySet()
        .stream()
        .map(el -> el.getKey() == 0 ? String.format("Evens: %s", el.getValue()) :
            String.format("Odds: %s", el.getValue()))
        .collect(Collectors.joining("\n"));

    System.out.println(result);

//                IntStream.range(0,10)
//                        .mapToObj(i -> r.nextInt(41) - 20)
//                        .forEach(el -> System.out.printf(" < %d > ",el));
  }
}
