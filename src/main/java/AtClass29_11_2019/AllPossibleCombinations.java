package AtClass29_11_2019;

import java.util.Arrays;
import java.util.List;

public class AllPossibleCombinations {
  public static void main(String[] args) {
    new AllPossibleCombinations()
        .allPossibleCombinations(Arrays.asList('A', 'B', 'C', 'D', 'E', 'F'));
  }

  public void allPossibleCombinations(List<Character> letters) {
    letters.stream()
        .flatMap(l1 -> letters.stream()
            .filter(l2 -> !l1.equals(l2))
            .map(l2 -> "" + l1 + l2)
        )
        .forEach(System.out::println);
  }
}
