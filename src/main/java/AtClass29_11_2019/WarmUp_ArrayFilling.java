package AtClass29_11_2019;

public class WarmUp_ArrayFilling {
  public static void main(String[] args) {
  }

  public int[] gen(int n) {
    int[] res = new int[n];
    int counter = n / 2 - (1 - n % 2);

    for (int i = 0; i < n / 2; i++) {
      res[i] = counter;
      res[n - 1 - i] = counter;
      counter--;
    }

    return res;
  }
}
