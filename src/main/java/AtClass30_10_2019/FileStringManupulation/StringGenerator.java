package AtClass30_10_2019.FileStringManupulation;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringGenerator {

  static String generate(int min, int max) {
    String allowedChars = "abcdefghijklmnopqrstvuwxyz";
    allowedChars += allowedChars.toUpperCase();
    StringBuilder res = new StringBuilder("");
    Random r = new Random();
    int len = r.nextInt(max + 1) + min;
    for (int i = 0; i < len; i++) {
      res.append(
          allowedChars.charAt(
              r.nextInt(allowedChars.length())));
    }
    return res.toString();
  }

  public static List<String> generateList(int length, int min, int max) {
    return Stream.generate(() -> generate(min, max))
        .limit(length)
        .collect(Collectors.toList());
  }

  public static void writeListToFile(List<String> strs, File file) throws IOException {
    BufferedWriter output = new BufferedWriter(new FileWriter(file));
    for (String s : strs) {
      output.write(s);
      output.newLine();
    }
    output.close();

  }

  public static List<String> getListFromFile(File file) throws IOException {
    BufferedReader input = new BufferedReader(new FileReader(file));
    List<String> list = input.lines()
        .collect(Collectors.toList());
    input.close();
    return list;
  }

  public static List<String> getSortedListFromFile(File file) throws IOException {
    return getListFromFile(file).stream()
        .sorted(Comparator.comparingInt(String::length))
        .collect(Collectors.toList());
  }

  public static void main(String[] args) throws IOException {
    int min = 10, max = 30, length = 30;
    List<String> list = generateList(length, min, max);

    File file1 = new File("data", "/file1.txt");
    writeListToFile(list, file1);
    List<String> sortedList = getSortedListFromFile(file1);

    File file2 = new File("data", "/sortedStrings.txt");

    writeListToFile(sortedList, file2);

    System.out.println("All done check files");
  }
}
