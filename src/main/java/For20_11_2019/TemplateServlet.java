package For20_11_2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public class TemplateServlet extends HttpServlet {
//  private final static Logger log = LoggerFactory.getLogger(TemplateServlet.class);
  private final TemplateEngine templateEngine;

  public TemplateServlet(TemplateEngine te) {
    this.templateEngine = te;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    HashMap<String, Object> data = new HashMap<>();
    data.put("name", "some name");
    data.put("numbers", Arrays.asList("1","2","3","4"));
//    log.
    templateEngine.render("temp.ftl",data,resp);


  }
}
