package For20_11_2019.parser;

import java.util.Map;

public class Parser {
  private static final char opening = '<';
  private static final char closing = '>';

  public static String parse(String input, Map<String, String> data) {
    StringBuilder res = new StringBuilder();
    for (int i = 0; i < input.length(); i++) {
      char c = input.charAt(i);
      if (c == opening) {
        StringBuilder keyBuilder = new StringBuilder();

        while (c != closing) {
          i++;
          c = input.charAt(i);
          keyBuilder.append(c);
        }

        keyBuilder.deleteCharAt(keyBuilder.length() - 1);
        String key = keyBuilder.toString();
        res.append(data.get(key));

      } else {
        res.append(c);
      }
    }
    return res.toString();
  }

}
