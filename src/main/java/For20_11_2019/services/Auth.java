package For20_11_2019.services;

public interface Auth {
  boolean login(String username, String pass);

  boolean register(String username, String nickname);

  String getAll();
}
