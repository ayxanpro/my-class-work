package For20_11_2019.services;


import java.sql.Connection;
import java.sql.DriverManager;

public class DataBaseConnection {
  private static final String url = "jdbc:mysql://localhost:5432/test_db?useSSL=false";
  private static final String username = "admin";
  private static final String password = "123";

  static Connection conn;

  public static Connection getDataBaseConnection() {
    try {
      Class.forName("com.mysql.jdbc.Driver");
      conn = DriverManager.getConnection(url, username, password);
    } catch (Exception e) {
      //...
    }
    return conn;
  }
}
