package For20_11_2019.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SQLInteractor {
  private final String insert = "INSERT INTO users VALUE (NULL ,?,?);";
  private final String select = "SELECT * FROM users;";

  public int insertUser(String name, String nickname) {
    System.out.println(name + " " + nickname);
    try (PreparedStatement statement = DataBaseConnection.getDataBaseConnection().prepareStatement(insert)) {
      statement.setString(1, name);
      statement.setString(2, nickname);
      return statement.executeUpdate();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return 0;
  }

  public String selectAll() {
    try (PreparedStatement statement = DataBaseConnection.getDataBaseConnection().prepareStatement(select)) {
      ResultSet resultSet = statement.executeQuery();
      StringBuilder res = new StringBuilder();
      while (resultSet.next()) {
        String first_name = resultSet.getString("first_name");
        String nickname = resultSet.getString("nickname");
        int id = resultSet.getInt("id");
        res.append(String.format("<<%d %s %s>>\n", id, first_name, nickname));
        return res.toString();
      }
    } catch (Exception e) {

    }
    return "null";
  }
}
