package For20_11_2019.services;

public class UserSQLService implements Auth {
  private final SQLInteractor data;

  public UserSQLService() {
    this.data = new SQLInteractor();
  }

  @Override
  public boolean login(String username, String pass) {
    return false;
  }

  @Override
  public boolean register(String username, String nickname) {
    int res = data.insertUser(username, nickname);
    return res > 0;
  }

  @Override
  public String getAll() {
    return data.selectAll();
  }


}
