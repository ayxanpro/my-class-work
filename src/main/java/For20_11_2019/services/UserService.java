package For20_11_2019.services;

import java.util.HashMap;
import java.util.Map;

public class UserService implements Auth {
  private Map<String, String> userData = new HashMap<String, String>();

  public UserService() {
    userData.put("test", "test");
    userData.put("user1", "user1");
  }

  public boolean login(String username, String pass) {
    return userData.containsKey(username) && userData.get(username).equals(pass);
  }

  @Override
  public boolean register(String username, String nickname) {
    return false;
  }

  @Override
  public String getAll() {
    return "null";
  }

}
