package For20_11_2019.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ByeWarmUp extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String name = req.getPathInfo().replaceFirst("/", "").split("/")[0];
    String res = String.format("<h1>Bye %s(</h1>", name);
    PrintWriter writer = resp.getWriter();
    writer.println(res);
  }
}
