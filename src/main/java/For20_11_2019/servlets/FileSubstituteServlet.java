package For20_11_2019.servlets;

import For20_11_2019.parser.Parser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Collectors;

public class FileSubstituteServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    File file = new File("files", "some.txt");
    HashMap<String, String> data = new HashMap<>();

    data.put("name", "Aykhan");
    data.put("surname", "Hasanov");

    BufferedReader br = new BufferedReader(new FileReader(file));

    String res = br.lines().collect(Collectors.joining("\n"));
    res = Parser.parse(res, data);

    br.close();
    resp.getWriter().println(res);
  }

}
