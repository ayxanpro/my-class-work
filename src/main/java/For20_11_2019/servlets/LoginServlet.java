package For20_11_2019.servlets;

import For20_11_2019.services.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class LoginServlet extends HttpServlet {

  private final Auth userService;

  public LoginServlet(Auth userData) {
    this.userService = userData;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Files.copy(Paths.get("./files/default.html"), resp.getOutputStream());
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String user_name = req.getParameter("user_name");
    String user_password = req.getParameter("user_password");
    try (PrintWriter w = resp.getWriter()) {

      if (userService.login(user_name, user_password)) {
        w.printf("%s has successfully logged in", user_name);
      } else {
        w.println("Wrong username or password");
      }

    }
  }

}
