package For20_11_2019.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class RedirectPage extends HttpServlet {
  private final String to;

  public RedirectPage(String to) {
    this.to = to;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println(req.getPathInfo());
    resp.sendRedirect(to);
  }

}
