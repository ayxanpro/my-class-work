package For20_11_2019.servlets;

import For20_11_2019.TemplateEngine;
import For20_11_2019.services.Auth;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

public class RegisterServlet extends HttpServlet {
  private final Auth auth;
  private final TemplateEngine te;

  public RegisterServlet(Auth auth, TemplateEngine te) {
    this.auth = auth;
    this.te = te;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Files.copy(Paths.get("./files", "register.html"), resp.getOutputStream());
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String nickname = req.getParameter("nickname");
    String first_name = req.getParameter("first_name");

    boolean result = auth.register(first_name, nickname);
    if (result) {
      HashMap<String, Object> data = new HashMap<String, Object>() {{
        put("first_name", first_name);
        put("nickname", nickname);
      }};
      te.render("userDisplay.html", data, resp);
    } else {
      resp.sendRedirect("/register");
    }

  }
}
