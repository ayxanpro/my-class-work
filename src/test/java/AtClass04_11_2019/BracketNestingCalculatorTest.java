package AtClass04_11_2019;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BracketNestingCalculatorTest {
        private BracketNestingCalculator app;

        @Before
        public void setUp() throws Exception {
                app = new BracketNestingCalculator();
        }

        @Test
        public void calc1() {
                assertEquals(1, app.calc("()()()()()"));
        }

        @Test
        public void calc2() {
                assertEquals(2, app.calc("(()()()()())"));
        }

        @Test
        public void calc3() {
                assertEquals(3, app.calc("((()))"));
        }

        @Test
        public void calc4() {
                assertEquals(3, app.calc("((()))(()()())"));
        }

        @Test
        public void calc5() {
                assertEquals(4, app.calc("((()))(()()())(((())))"));
        }
}