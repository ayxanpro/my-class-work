package AtClass04_11_2019;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FindingMissingBracketsTest {
        private FindingMissingBrackets app;

        @Before
        public void setUp(){
                app = new FindingMissingBrackets();
        }

        @Test
        public void calc1() {
                try {
                        app.calc("()()()())()");
                }catch (Exception e){
                        String res = e.getMessage();
                        assertEquals("Missing 0:(, 1:)", res);
                }
        }

        @Test
        public void calc2() {
                try {
                        app.calc("(()()()(()())");
                }catch (Exception e){
                        String res = e.getMessage();
                        assertEquals("Missing 1:(, 0:)", res);
                }
        }

        @Test
        public void calc3() {
                try {
                        app.calc("((())))");
                }catch (Exception e){
                        String res = e.getMessage();
                        assertEquals("Missing 0:(, 1:)", res);
                }
        }

        @Test
        public void calc3_1() {
                try {
                        app.calc("((())))(");
                }catch (Exception e) {
                        String res = e.getMessage();
                        assertEquals("Missing 1:(, 1:)", res);
                }
        }

        @Test
        public void calc3_2() {
                try {
                        app.calc("((()))))((())");
                }catch (Exception e) {
                        String res = e.getMessage();
                        assertEquals("Missing 1:(, 2:)", res);
                }
        }

        @Test
        public void calc4() {
                assertEquals(3, app.calc("((()))(()()())"));
        }

        @Test
        public void calc5() {
                assertEquals(4, app.calc("((()))(()()())(((())))"));
        }

}