package AtClass04_12_2019.warmup;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SolutionTest {
  private Solution cs;

  @Before
  public void setUp() throws Exception {
    cs = new Solution();
  }

  @Test
  public void solve1() {
    assertEquals("abc", cs.solve("aabbcc"));
  }

  @Test
  public void solve2() {
    assertEquals("abcsed", cs.solve("abcseda"));
  }

  @Test
  public void solve3() {
    assertEquals("dsjw", cs.solve("dsjwjs"));
  }

  @Test
  public void solve4() {
    assertEquals("", cs.solve(""));
  }

  @Test
  public void solve5() {
    assertEquals("a", cs.solve("aaa"));
  }
}