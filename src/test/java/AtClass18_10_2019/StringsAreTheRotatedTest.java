package AtClass18_10_2019;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringsAreTheRotatedTest {
        private StringsAreTheRotated tester;
        @Before
        public void bf()
        {
                tester = new StringsAreTheRotated();
        }
        @Test
        public void check_one_null() {
                String a = null;
                String b = "asd";
                boolean expected = false;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_second_null() {
                String a = "bad";
                String b = null;
                boolean expected = false;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_same_string() {
                String a = "bad";
                String b = "bad";
                boolean expected = true;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_shifted_strings() {
                String a = "bad";
                String b = "adb";
                boolean expected = true;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_shifted_many_times() {
                String a = "HelloWorld";
                String b = "WorldHello";
                boolean expected = true;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_different() {
                String a = "bad";
                String b = "asd";
                boolean expected = false;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_different_2() {
                String a = "abc";
                String b = "bac";
                boolean expected = false;
                assertEquals(expected,tester.check(a,b));
        }
        @Test
        public void check_shifted_many_times_2() {
                String a = "HelloWorld";
                String b = "World Hello";
                boolean expected = false;
                assertEquals(expected,tester.check(a,b));
        }
}