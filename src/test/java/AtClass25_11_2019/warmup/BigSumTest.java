package AtClass25_11_2019.warmup;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BigSumTest {
  private BigSum obj;
  @Before
  public void setUp() throws Exception {
      obj = new BigSum();
  }

  @Test
  public void ssum() {
    assertEquals("123",obj.Ssum("62","61"));
  }
  @Test
  public void ssum1() {
    assertEquals("400",obj.Ssum("342","58"));
  }
  @Test
  public void ssu2() {
    assertEquals("323",obj.Ssum("162","161"));
  }
  @Test
  public void ssum3() {
    assertEquals("1000",obj.Ssum("999","1"));
  }
  @Test
  public void ssum4() {
    assertEquals("1415",obj.Ssum("15","1400"));
  }
  @Test
  public void ssum5() {
    assertEquals("24124",obj.Ssum("24004","120"));
  }
  @Test
  public void ssum6() {
    assertEquals("101",obj.Ssum("101","0"));
  }
}