package AtClass28_10_2019.NumberSeperator;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class SolutionTest {
        Solution core;
        @Before
        public void init(){
                core = new Solution();
        }
        @Test
        public void emptyString(){
                int expected = 0;
                int actual = core.add("");
                assertThat(actual,equalTo(expected));
        }
        @Test
        public void allNumber(){
                int expected = 6;
                int actual = core.add("1 2 3");
                assertThat(actual,equalTo(expected));
        }
        @Test
        public void partialNumber(){
                int expected = 4;
                int actual = core.add("1 2a 3");
                assertThat(actual,equalTo(expected));
        }
        @Test
        public void partialNumber2(){
                int expected = 3;
                int actual = core.add("1a 2a 3");
                assertThat(actual,equalTo(expected));
        }
        @Test
        public void partialNumbe3(){
                int expected = 5;
                int actual = core.add("a1 2 3");
                assertThat(actual,equalTo(expected));
        }
        @Test
        public void allString(){
                int expected = 0;
                int actual = core.add("asd fds");
                assertThat(actual,equalTo(expected));
        }
}