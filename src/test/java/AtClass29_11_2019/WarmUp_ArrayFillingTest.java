package AtClass29_11_2019;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class WarmUp_ArrayFillingTest {
  private WarmUp_ArrayFilling test_class;

  @Before
  public void setUp() throws Exception {
    test_class = new WarmUp_ArrayFilling();
  }

  @Test
  public void even1() {
    int[] res = test_class.gen(8);
    System.out.println(Arrays.toString(res));
    assertArrayEquals(new int[]{3, 2, 1, 0, 0, 1, 2, 3},res);
  }

  @Test
  public void even2() {
    assertArrayEquals(new int[]{2, 1, 0, 0, 1, 2}, test_class.gen(6));
  }

  @Test
  public void odd1() {
    assertArrayEquals(new int[]{4, 3, 2, 1, 0, 1, 2, 3, 4}, test_class.gen(9));
  }
}